name             'samba4'
maintainer       'y-yoshinoya'
maintainer_email 'y-yoshinoya@ase.co.jp'
license          'All rights reserved'
description      'Installs/Configures samba4'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

supports "ubuntu"

depends "apt"
depends "build-essential"
