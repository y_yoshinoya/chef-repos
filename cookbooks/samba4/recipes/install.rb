include_recipe 'apt::default'
include_recipe 'build-essential::default'

samba_version = node[:samba4][:version]
samba_archive = "samba-#{samba_version}.tar.gz"
url = "#{node[:samba4][:samba_url]}#{samba_archive}"
working_directory = node[:samba4][:working_directory]
install_path = node[:samba4][:install_path]
samba_path = "#{install_path}/sbin/samba"
configure_options = "--prefix=#{install_path} #{node[:samba4][:add_configure_options]}"

# https://wiki.samba.org/index.php/Samba_4/OS_Requirements
required_packages = %W{
  libacl1-dev libattr1-dev libblkid-dev libgnutls-dev libreadline-dev python-dev
  python-dnspython gdb pkg-config libpopt-dev libldap2-dev dnsutils libbsd-dev
  attr krb5-user docbook-xsl libpam0g-dev acl sysv-rc-conf
}
required_packages.each{|name| package(name)}

# https://wiki.samba.org/index.php/Build_Samba
execute "wget #{url}" do
  cwd working_directory
  not_if { ::File.exists?("#{working_directory}/#{samba_archive}") }
end

execute 'make and install samba4' do
  cwd working_directory
  command <<-EOT
    tar xzvf #{samba_archive}
    cd #{::File.basename(samba_archive, '.tar.gz')}
    ./configure #{configure_options}
    make
    make install
  EOT
  not_if { ::File.exists?(samba_path) }
end

bash "set PATH" do
  not_if "cat /etc/profile | grep #{install_path}/sbin"
  code <<-EOT
    echo "export PATH=#{install_path}/bin:#{install_path}/sbin:$PATH" >> /etc/profile
    source /etc/profile
  EOT
end

link "/etc/samba" do
  to "#{install_path}/etc"
  link_type :symbolic
  action :create
end

# https://wiki.samba.org/index.php/Samba4/InitScript
template "/etc/init.d/samba4" do
  source "samba4.init.erb"
  mode "0755"
  variables({
    :samba_path => samba_path
  })
end
execute "sysv-rc-conf samba4 on" do
  not_if { ::File.exists?("/etc/rc2.d/S20samba4") }
end

